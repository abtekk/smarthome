//#define BOARD_USES_WIFI
//#define USES_API

#ifdef BOARD_USES_WIFI
  extern const char* ssid = "";
  extern const char* password =  "";
#endif

#define BOARD_HAS_DHT22

#ifdef BOARD_HAS_DHT22
  #define DHTTYPE DHT22
#endif

#ifdef USES_API
  extern const String API_SERVER = "https://192.168.1.208/test.php";
#endif