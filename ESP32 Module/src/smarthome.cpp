#include <Arduino.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include "pins.h"
#include "configuration.h"

DHT dht(DHTPIN, DHTTYPE);
void setup()
{
    // Initialize serial port
    Serial.begin(9600);
    
    #ifdef BOARD_USES_WIFI
      WiFi.begin(ssid, password);
      delay(4000); 
      while (WiFi.status() != WL_CONNECTED) { //Check for the connection
        delay(1000);
        Serial.println("Connecting to WiFi..");
      }
    #endif
    
    Serial.println(F("DHT22 temperature and humidity sensor example\n"));

    pinMode(GND2,OUTPUT);//define a digital pin as output
    digitalWrite(GND2, LOW);// set the above pin as LOW so it acts as Ground  
    
    // Initialize sensor
    dht.begin();
    delay(4000); //We delay for a few seconds here to allow the DHT22 to "wake up".
    
}

void loop()
{
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
      Serial.println("Unable to read from DHT22 Sensor!");
      return;
    }
  
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" °C ");
  int mq135 = analogRead(MQ135PIN);
  Serial.print("AirQua=");
  Serial.print(mq135, DEC);
  Serial.println(" PPM");

  #ifdef USES_API
    if (WiFi.status() == WL_CONNECTED) {
      HTTPClient http;
      http.begin(API_SERVER);
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");

      int postTemperature = http.POST("sensor=temperature&value="+String(t)+"&hostname=living_room");
      Serial.println("Sent temperature to API.");

      delay(1000);

      http.begin(API_SERVER);
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");
      
      int postHumidity = http.POST("sensor=humidity&value="+String(h)+"&hostname=living_room");
      Serial.println("Sent humidity to API.");

      http.begin(API_SERVER);
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");
      
      int postAirQual = http.POST("sensor=air_quality&value="+String(mq135)+"&hostname=living_room");
      Serial.println("Sent humidity to API.");

      if (postTemperature < 0 || postHumidity < 0 || postAirQual < 0) {
        Serial.println("API Error: ");
        Serial.print("Temp: " + String(postTemperature) + " ");
        Serial.print("Humidity: " + String(postHumidity) + " ");
        Serial.print("Air Qual: " + String(postAirQual) + " ");
      }

      http.end();
    }
  #endif
  
  delay(2000);
}
